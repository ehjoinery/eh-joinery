EH Joinery (Edwards & Hampson) are based on Liverpool and work throughout the UK.
We specialise in wooden staircase design, manufacture and fitting. We also manufacture Bifold doors and windows. We can match existing timber work and match oak, ash mahogany.

Address: 194-196 Rimrose Road Bootle, Liverpool, Merseyside L20 4QS, UK

Phone: +44 151 933 3191
